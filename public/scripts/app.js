/*
  main script for only runs every function
*/

//Custom min chars to search
new Awesomplete( '.search-form input[type=text]', {
	minChars: 3,
	maxItems: 15
});	

searchForm();

var wrapApp = {
	searchfrom: function(){
		//Var to input
		var input_data = $('.search-form input[type=text]');
		var input_send = $('.search-form input[type="submit"]');

		// On/Off Send Button
		input_data.keyup(function () {
			var count_char = $(this).val().length;
			if (count_char > 2){
				input_send.attr('disabled', false);
			}
			else{ input_send.attr('disabled', true); }
		});
	}
}

$( document ).ready(function() {
	wrapApp.searchfrom();
});

$( window ).resize(function() {
});
